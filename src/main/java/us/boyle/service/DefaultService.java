package us.boyle.service;

import org.springframework.stereotype.Service;

@Service
public class DefaultService implements QualityService {

    public String welcomeMessage() {
        return "Welcome to Quality!";
    }
}
