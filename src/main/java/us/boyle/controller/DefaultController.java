package us.boyle.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import us.boyle.service.QualityService;

@RestController
public class DefaultController {

    private static Logger logger = LoggerFactory.getLogger(DefaultController.class);

    private final QualityService qualityService;

    public DefaultController(QualityService service) {
        qualityService = service;
    }

    @GetMapping("/")
    public String welcomeMessage() {
        return qualityService.welcomeMessage();
    }
}
