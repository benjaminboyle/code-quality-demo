package us.boyle.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DefaultServiceTest {

    private QualityService sut;

    @BeforeEach
    void setup() {
        sut = new DefaultService();
    }

    @Test
    void welcomeMessage_success() {
        assertThat(sut.welcomeMessage()).isEqualTo("Welcome to Quality!");
    }

    @Test
    void welcomeMessage_failure() {
        assertThat(sut.welcomeMessage()).isEqualTo("Welcome!");
    }
}
